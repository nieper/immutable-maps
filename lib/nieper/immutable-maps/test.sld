(define-library (nieper immutable-maps test)
  (export run-tests)
  (import (scheme base)
	  (srfi 27)
	  (srfi 64)
	  (srfi 128)
	  (nieper immutable-maps))
  (begin
    (define integers (make-comparator integer? = < #f))
    (define (make-random-list length n)
      (let loop ((length length))
	(if (= length 0)
	    '()
	    (cons (random-integer n) (loop (- length 1))))))
    
    (define (run-tests)
      (test-begin "Immutable maps")

      (test-assert "Creating an immutable map yields an immutable map"
		   (imap? (imap integers)))

      (test-assert "Immutable maps contain their added elements"
		   (imap-contains? (imap-replace (imap integers) 42 'x)
				   42))

      (test-assert "Immutable maps only contain their added elements"
		   (not (imap-contains? (imap-replace (imap integers) 42 'x)
					43)))

      (test-assert "Inserting random elements in list"
		   (let*
		       ((random-list
			 (make-random-list 1000 1000000))
			(map
			 (let loop ((map (imap integers))
				    (random-list random-list))
			   (if (null? random-list)
			       map
			       (loop (imap-replace map (car random-list) #f)
				     (cdr random-list))))))
		     (let loop ((random-list random-list))
		       (or (null? random-list)
			   (and (imap-contains? map (car random-list))
				(loop (cdr random-list)))))))
      
      (test-assert "Deleting random elements in list"
		   (let*
		       ((random-list
			 (make-random-list 1000 1000000))
			(map
			 (let loop ((map (imap integers))
				    (random-list random-list))
			   (if (null? random-list)
			       map
			       (loop (imap-replace map (car random-list) #f)
				     (cdr random-list))))))
		     (let loop ((random-list random-list) (map map))
		       (or (null? random-list)
			   (let ((map (imap-delete map (car random-list))))
			     (and (not (imap-contains? map (car random-list)))
				  (loop (cdr random-list) map)))))))
      
      (test-end))))
