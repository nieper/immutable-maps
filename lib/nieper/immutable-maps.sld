(define-library (nieper immutable-maps)
  (export imap
	  imap?
	  imap-replace
	  imap-delete
	  imap-contains?
	  imap-empty?
	  imap-search)
  (import (scheme base)
	  (scheme case-lambda)
	  (srfi 2)
	  (srfi 128))
  (include "immutable-maps.scm"))
