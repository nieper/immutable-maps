(import (scheme base)
	(srfi 64)
	(rename (nieper immutable-maps test) (run-tests
					      run-immutable-maps-test)))

(test-begin "immutable-maps")

(run-immutable-maps-test)

(test-end)
