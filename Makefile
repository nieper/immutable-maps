SCHEME = chibi-scheme -Ilib

check:
	$(SCHEME) tests.scm

.PHONY: check
